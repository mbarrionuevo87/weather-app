import React from 'react';
import { Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import Dashboard from '../components/Dashboard';

export const history = createHistory();

const AppRouter = (props) => (
	<Router history={history}>
		<div>
			<Switch>
				<Route path="/" component={Dashboard} exact={true}/>
			</Switch>
		</div>
	</Router>
)

export default AppRouter;