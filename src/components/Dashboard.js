import React from 'react';
import TodayWeather from './TodayWeather';


class Dashboard extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			todayWeather: undefined
		}
	}

	componentDidMount(){
		const todayWeatherUrl = `http://api.openweathermap.org/data/2.5/weather?id=3844421&lang=es&units=metric&APPID=${process.env.REACT_APP_WEATHER_API}`;	
		fetch(todayWeatherUrl).then( (response) => {
			return response.json();
		}).then( (myJson) => {
			this.setState( () => {
				return {
					todayWeather: myJson
				}
			})
		}).catch( (error) => {
			console.log('Error', error)
		})
	}

	render(){
		const todayWeather = this.state.todayWeather;
		return(
			<div>
				{this.state.todayWeather && <TodayWeather todayWeather={{todayWeather}} />}
			</div>
		)
	}
}

export default Dashboard;