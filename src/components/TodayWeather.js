import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Button from 'antd/lib/button';
import { Row, Col } from 'antd';



const TodayWeather = ({todayWeather}) => {


	const data = todayWeather.todayWeather; 
	const sunrise = moment(data.sys.sunrise).format("h:mm:ss a");
	const sunset = moment(data.sys.sunset).format("h:mm:ss a");

	return(
		<div className="card">

			<Row className="title">
				<Col span={24}>
					<span className="title">{data.name}</span>
				</Col>
			</Row>
			<Row className="main">
				<Col span={24}>
					<i className={`icon owi owi-${data.weather[0].icon}`}></i>
					<div className="main__temp">{data.main.temp}°</div>
					<div className="main__desc">{data.weather[0].description}</div>
				</Col>
			</Row>
			<Row className="temps">
				<Col span={12}>
					<div>Min:</div>
					<span className="minmax">{data.main.temp_min}°</span>
				</Col>
				<Col span={12}>
					<div>Max:</div>
					<span className="minmax">{data.main.temp_max}°</span>
				</Col>
			</Row>
			<Row className="temps">
				<Col span={12}>
					Sunrise: {sunrise}
				</Col>
				<Col span={12}>
					Sunset: {sunset}
				</Col>
			</Row>							
		</div>
	)
}


export default TodayWeather;


