import React, { Component } from 'react';
import { BrowserRoutes } from 'react-router-dom';
import AppRouter from './routers/AppRouter';
import './styles/styles.scss';

const day = {
	day: 'Monday'
};

class App extends Component {

	render() {
		return (
			<div>
		  		<AppRouter day={day}/>
		  	</div>
		);
	}
}

export default App;
